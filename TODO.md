# TODO

This is a unordered list of features and taks for the Manette project.

## Features

* Hide hidden files by default
* Allow to run ls -l
* Allow to run ls -a
* Escape to return to list view
* Open files with xdg-open if exists

## Improvements

* Run commands in user shell
* Escape auto completion

## Bugs

* Running mpv gets stuck
