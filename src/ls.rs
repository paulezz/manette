/*

Copyright or © or Copr. Paul Ezvan (2023)

paul@ezvan.fr

This software is a computer program whose purpose is to provide a terminal file explorer.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
use crate::command::run;
use crate::env;
use crate::file::filetype;
use crate::file::filetype::FileType;
use crate::ui::update;
use arg::Args;
use cursive::Cursive;
use std::cmp::Ordering;
use std::fs;
use std::path::Path;

pub struct FileEntry {
    pub filename: String,
    pub filetype: FileType,
}

impl Ord for FileEntry {
    fn cmp(&self, other: &Self) -> Ordering {
        self.filename.cmp(&other.filename)
    }
}

impl PartialOrd for FileEntry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for FileEntry {
    fn eq(&self, other: &Self) -> bool {
        self.filename == other.filename
    }
}

impl Eq for FileEntry {}

#[derive(Args, Debug)]
pub struct LsArgs {
    #[arg(short, long)]
    all: bool,
    path: Option<String>,
}

pub struct Ls {}

impl Ls {
    pub fn run(params: Vec<&str>, command: &str, s: &mut Cursive) {
        let args = match LsArgs::from_args(params) {
            Ok(args) => args,
            Err(_err) => {
                run::run_shell(command, s);
                return;
            }
        };
        let dir = match args.path {
            None => String::from("./"),
            Some(path) => path,
        };
        match fs::read_dir(dir.as_str()) {
            Ok(paths) => {
                let mut path_strings: Vec<FileEntry> = Vec::new();

                for entry in paths {
                    match entry {
                        Ok(entry) => {
                            let path = entry.path();
                            let path = match path.strip_prefix(dir.as_str()) {
                                Ok(path_without_prefix) => path_without_prefix.to_path_buf(),
                                Err(error) => {
                                    log::error!(
                                        "Cannot remove prefix from {:?}: {:?}",
                                        path,
                                        error
                                    );
                                    path
                                }
                            };
                            let path = path.to_str();
                            match path {
                                Some(path) => {
                                    // filter hidden files
                                    if !args.all && path.starts_with(".") {
                                        continue;
                                    }
                                    let filetype: FileType = filetype::get_type(entry);
                                    path_strings.push(FileEntry {
                                        filename: path.to_string(),
                                        filetype,
                                    });
                                }
                                None => {
                                    update::show_error(
                                        s,
                                        format!("Error converting path to string: {:?}", entry),
                                    );
                                }
                            }
                        }
                        Err(error) => {
                            update::show_error(s, format!("Error listing path: {:?}", error));
                        }
                    }
                }

                path_strings.sort();
                match env::current_dir() {
                    Ok(current_dir) => {
                        if current_dir != Path::new("/").to_path_buf() {
                            path_strings.insert(
                                0,
                                FileEntry {
                                    filename: "..".to_string(),
                                    filetype: FileType::Directory,
                                },
                            );
                        }
                    }
                    Err(error) => log::error!("Cannot get current directory: {:?}", error),
                }
                update::file_list_view(s, path_strings);
            }
            Err(error) => {
                update::show_error(s, error.to_string());
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use cursive;

    #[test]
    fn test_ls() {
        let mut test_cursive = cursive::dummy();
        let test_params = vec!["."];
        let test_cmd = "ls .";
        Ls::run(test_params, test_cmd, &mut test_cursive);
    }
}
